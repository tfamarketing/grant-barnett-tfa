<?php
return [
    "title-section" => [
        "title" => "Ladies\nSliders",
        "text"  => "Grant Barnett are now supplying embellished and branded sliders. These are a footwear staple that your customers need and want to see as part of your range.",
        "product1" => "/images/footwear/falling-products/BlackWhite.png?123",
        "product2" => "/images/footwear/falling-products/PinkShiny.png?123",
        "product3" => "/images/footwear/falling-products/PinkShiny2.png?123",
        "image1" => "/images/footwear/10.jpg?123",
        "image2" => "/images/footwear/11.jpg?123",
        "optional-text" => "With a lightweight, moulded foot-bed for comfort, and styles that wouldn’t look out of place either poolside or on rainy day walks, these are a holiday must and an everyday essential.",
        "optional-product" => "/images/footwear/falling-products/WhiteColour.png?123",
        "optional-image" => "/images/footwear/12.jpg?123"
    ],
    "6-shoes" => [
        "shoes" => [
            "tl" => '/images/footwear/adults-sliders/BlueBow.png?123',
            "tm" => '/images/footwear/adults-sliders/GoldShiny.png?123',
            "tr" => '/images/footwear/adults-sliders/BlackShiny.png?123',
            "bl" => '/images/footwear/adults-sliders/ShinySilver.png?123',
            "bm" => '/images/footwear/adults-sliders/WhiteShiny.png?123',
            "br" => '/images/footwear/adults-sliders/BlackShinyGems.png?123',
        ],
        'text' => "Fugiat aliqua aute proident exercitation excepteur cupidatat elit exercitation laborum pariatur nulla et ea veniam.",
        "text-side" => "left"
    ]
];