<?php
return [
    "title-section" => [
        "title" => "Children's\nSliders",
        "text"  => "These children sliders are a real statement piece. With vibrant colours and stylish looks, Your range will certainly stand out to your customers. ",
        "product1" => "/images/footwear/falling-products/pinkpow.png?123",
        "product2" => "/images/footwear/falling-products/BlueJelly.png?123",
        "image1" => "/images/footwear/1.jpg?123",
        "image2" => "/images/footwear/2.jpg?123",
        "optional-text" => "These lightweight sliders are designed to ensure they mould to the Child’s feet for increased comfort and stability.",
        "optional-product" => "/images/footwear/falling-products/GoldJelly.png?123",
        "optional-product2" => "/images/footwear/falling-products/Shiny.png?123",
        "optional-image" => "/images/footwear/3.jpg?123"
    ],
    "6-shoes" => [
        "shoes" => [
            "tl" => '/images/footwear/childrens-sliders/WhitePink.png?123',
            "tm" => '/images/footwear/childrens-sliders/BlueBow.png?123',
            "tr" => '/images/footwear/childrens-sliders/Blue.png?123',
            "bl" => '/images/footwear/childrens-sliders/BlueRedWhite.png?123',
            "bm" => '/images/footwear/childrens-sliders/GreyShiny.png?123',
            "br" => '/images/footwear/childrens-sliders/Yellow.png?123',
        ],
        'text' => "Fugiat aliqua aute proident exercitation excepteur cupidatat elit exercitation laborum pariatur nulla et ea veniam. Officia in officia veniam consectetur. Adipisicing magna sint nulla dolor.",
        "text-side" => "right"
    ]
];