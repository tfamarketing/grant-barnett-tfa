<?php
return [
    "title-section" => [
        "title" => "Ladies\nSandals",
        "text"  => "Jellies are the epitome of summer style for children and adults, revamped by our design team with studs and stylish touches for a modern take on classic footwear.",
        "product1" => "/images/footwear/falling-products/PurpleJelly.png?123",
        "product2" => "/images/footwear/falling-products/BrownShiny.png?123",
        "product3" => "/images/footwear/falling-products/BrownShiny2.png?123",
        "image1" => "/images/footwear/7.jpg?123",
        "image2" => "/images/footwear/8.jpg?123",
        "optional-text" => "These sandals are a must-have in any shoe collection and you certainly can’t go wrong with ours.",
        "optional-product" => "/images/footwear/falling-products/Green.png?123",
        "optional-image" => "/images/footwear/9.jpg?123"
    ],
    "6-shoes" => [
        "shoes" => [
            "tl" => '/images/footwear/adults-sandals/Purplejellyflat.png?123',
            "tm" => '/images/footwear/adults-sandals/BlackGems.png?123',
            "tr" => '/images/footwear/adults-sandals/PinkPurpleGreen.png?123',
            "bl" => '/images/footwear/adults-sandals/PinkFluffy.png?123',
            "bm" => '/images/footwear/adults-sandals/Red.png?123',
            "br" => '/images/footwear/adults-sandals/BlackHeel.png?123',
        ],
        'text' => "Fugiat aliqua aute proident exercitation excepteur cupidatat elit exercitation laborum pariatur nulla et ea veniam. Officia in officia veniam consectetur.",
        "text-side" => "left"
    ]
];