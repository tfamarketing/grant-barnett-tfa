<?php
return [
    "pre-title-section" => [
        "left-product" => "/images/footwear/falling-products/GreenMonkey.png?123",
        "right-product" => "/images/footwear/falling-products/OrangeTiger.png?123",
        "image" => "/images/footwear/4.jpg?123"
    ],
    "title-section" => [
        "title" => "Children's\nClogs",
        "text"  => "Grant Barnett’s playful kids’ clogs are stylish yet practical. They feature 3D ears, 3D patches, badges and glitter – what more could anyone want from a clog?",
        "image2" => "/images/footwear/falling-products/PurpleCat1.png?123",
        "product2" => "/images/footwear/falling-products/PurpleCat2.png?123",
        "image1" => "/images/footwear/5.jpg?123",
        "optional-text" => "Utilising EVA fabrication and featuring a pivoting heel strap, exceptional comfort comes as standard.",
        "optional-image" => "/images/footwear/6.jpg?123"
    ],
    "6-shoes" => [
        "shoes" => [
            "tl" => '/images/footwear/childrens-clogs/BlueShark.png?123',
            "tm" => '/images/footwear/childrens-clogs/RedDragon.png?123',
            "tr" => '/images/footwear/childrens-clogs/PinkDragon.png?123',
            "bl" => '/images/footwear/childrens-clogs/PinkIceCream.png?123',
            "bm" => '/images/footwear/childrens-clogs/BlueMonster.png?123',
            "br" => '/images/footwear/childrens-clogs/GreyDino.png?123',
        ],
        'text' => "Fugiat aliqua aute proident exercitation excepteur cupidatat elit exercitation laborum pariatur nulla et ea veniam. Officia in officia veniam consectetur. Adipisicing magna sint nulla dolor.",
        "text-side" => "right"
    ]
];