<?php

function footwearSection($title, $data) {
    $pathFormat = '%s/includes/%s.php';
    $path = realpath(sprintf($pathFormat, dirname(__FILE__), $title));
    if (file_exists($path)) {
        ob_start();
        include $path;
        return ob_get_clean();
    } else {
        return false;
    }
}

function getData($sector, $title) {
    $pathFormat = '%s/data/%s/%s.php';
    $path = realpath(sprintf($pathFormat, dirname(__FILE__), $sector, $title));
    if (file_exists($path)) {
        return include $path;
    } else {
        return false;
    }
}