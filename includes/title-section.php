<?php
$imagePath = realpath(dirname(__FILE__) . '/..' . $data['product1']);
list($width, $height, $type, $attr) = getimagesize($imagePath);
?>
<div class="col-12 title-section">
    <div class="row">
        <div class="col-6">
            <img class="image left product-1" src="<?= $data['product1'] ?>" <?= $attr ?> alt="">
            <div class="text left">
                <h2 class="txt-orange"><?= nl2br($data['title']) ?></h2>
                <hr>
                <p class="txt-white">
                    <?= $data['text'] ?>
                </p>
                <?php if ($data['title'] === "Children's\nClogs"): ?>
                <img class="optional-image" src="<?= $data['optional-image'] ?>" alt="" srcset="">
                <?php endif; ?>
            </div>
        </div>
        <div class="col-6 title-falling">
            <?php if ($data['title'] === "Children's\nClogs"): ?>
            <p class="text right txt-orange"><?= $data['optional-text'] ?></p>
            <?php endif; ?>
            <img class="fall-1 parallax" src="<?= $data['image1'] ?>" data-scroll-factor="6" alt="">
            <img class="fall-2 parallax" src="<?= $data['image2'] ?>" data-scroll-factor="-2" alt="">
            <img class="fall-3 parallax" src="<?= $data['product2'] ?>" data-scroll-factor="3" alt="">
            <img class="fall-4 parallax" src="<?= $data['product3'] ?>" data-scroll-factor="-3" alt="">
        </div>
    </div>
</div>

<?php
if ($data['title'] !== "Children's\nClogs" && array_key_exists('optional-text', $data)) {
    echo footwearSection('title-optional', $data);
}