<div class="<?= $title ?> row">
    <?= footwearSection('pre-title-section', $data['pre-title-section']); ?>
    <?= footwearSection('title-section', $data['title-section']); ?>
    <?= footwearSection('6-shoes', $data['6-shoes']); ?>
</div>