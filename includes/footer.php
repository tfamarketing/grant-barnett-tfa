<div class="clearfix" id="page">

<div class="clearfix colelem" id="pu28280"><!-- group -->
     <div class="browser_width grpelem" id="u28280-bw" style="height: 388px;">
      <div class="museBGSize" id="u28280"><!-- column -->
       <div class="clearfix" id="u28280_align_to_page">
        <div class="position_content" id="u28280_position_content">
         <!-- m_editable region-id="editable-static-tag-U28284-BP_infinity" template="umbrellas.html" data-type="image" -->
         <div class="clip_frame colelem" id="u28284" data-muse-uid="U28284" data-muse-type="img_frame"><!-- image -->
          <img class="block" id="u28284_img" src="images/grant%20barnet%20logo-02.png?crc=4076234817" alt="" width="34" height="45" data-muse-src="images/grant%20barnet%20logo-02.png?crc=4076234817">
         </div>
         <!-- /m_editable -->
         <div class="colelem" id="u28281"><!-- simple frame --></div>
         <div class="clearfix colelem" id="pu29141-4"><!-- group -->
          <!-- m_editable region-id="editable-static-tag-U29141-BP_infinity" template="umbrellas.html" data-type="html" data-ice-options="disableImageResize,link,clickable" data-ice-editable="link" -->
          <a class="nonblock nontext MuseLinkActive clearfix grpelem" id="u29141-4" href="umbrellas.html" data-href="page:U28064" data-muse-uid="U29141" data-muse-type="txt_frame"><!-- content --><p>umbrellas</p></a>
          <!-- /m_editable -->
          <!-- m_editable region-id="editable-static-tag-U29142-BP_infinity" template="umbrellas.html" data-type="html" data-ice-options="disableImageResize,link,clickable" data-ice-editable="link" -->
          <a class="nonblock nontext transition clearfix grpelem" id="u29142-4" href="accessories.html" data-href="page:U26397" data-muse-uid="U29142" data-muse-type="txt_frame"><!-- content --><p>accessories</p></a>
          <!-- /m_editable -->
          <!-- m_editable region-id="editable-static-tag-U29139-BP_infinity" template="umbrellas.html" data-type="html" data-ice-options="disableImageResize,link,clickable" data-ice-editable="link" -->
          <a class="nonblock nontext transition clearfix grpelem" id="u29139-4" href="process.html" data-href="page:U2353" data-muse-uid="U29139" data-muse-type="txt_frame"><!-- content --><p>our process</p></a>
          <!-- /m_editable -->
          <!-- m_editable region-id="editable-static-tag-U29137-BP_infinity" template="umbrellas.html" data-type="html" data-ice-options="disableImageResize,link,clickable" data-ice-editable="link" -->
          <a class="nonblock nontext transition clearfix grpelem" id="u29137-4" href="contact.html" data-href="page:U28573" data-muse-uid="U29137" data-muse-type="txt_frame"><!-- content --><p>contact</p></a>
          <!-- /m_editable -->
         </div>
         <div class="colelem" id="u28283"><!-- simple frame --></div>
         <div class="clearfix colelem" id="ppu28300"><!-- group -->
          <div class="clearfix grpelem" id="pu28300"><!-- column -->
           <div class="museBGSize colelem" id="u28300"><!-- simple frame --></div>
           <div class="museBGSize colelem" id="u28302"><!-- simple frame --></div>
          </div>
          <div class="clearfix grpelem" id="ppu28297-4"><!-- column -->
           <div class="clearfix colelem" id="pu28297-4"><!-- group -->
            <!-- m_editable region-id="editable-static-tag-U28297-BP_infinity" template="umbrellas.html" data-type="html" data-ice-options="disableImageResize,link" -->
            <div class="clearfix grpelem" id="u28297-4" data-muse-uid="U28297" data-muse-type="txt_frame"><!-- content -->
             <p>+44 (0) 1279 758075</p>
            </div>
            <!-- /m_editable -->
            <div class="museBGSize grpelem" id="u28301"><!-- simple frame --></div>
           </div>
           <!-- m_editable region-id="editable-static-tag-U28298-BP_infinity" template="umbrellas.html" data-type="html" data-ice-options="disableImageResize,link" -->
           <div class="clearfix colelem" id="u28298-4" data-muse-uid="U28298" data-muse-type="txt_frame"><!-- content -->
            <p><a href="mailto:enquiries@grantbarnett.com" class="contact-link">enquiries@grantbarnett.com</a></p>
           </div>
           <!-- /m_editable -->
          </div>
          <!-- m_editable region-id="editable-static-tag-U28299-BP_infinity" template="umbrellas.html" data-type="html" data-ice-options="disableImageResize,link" -->
          <div class="clearfix grpelem" id="u28299-8" data-muse-uid="U28299" data-muse-type="txt_frame"><!-- content -->
           <p>Waterfront House, 56-61 South Street,</p>
           <p>Bishops Stortford, Hertfordshire,</p>
           <p>CM23 3AL</p>
          </div>
          <!-- /m_editable -->
         </div>
         <div class="colelem" id="u28282"><!-- simple frame --></div>
         <div class="clearfix colelem" id="pu28296-4"><!-- group -->
          <!-- m_editable region-id="editable-static-tag-U28296-BP_infinity" template="umbrellas.html" data-type="html" data-ice-options="disableImageResize,link" -->
          <div class="clearfix grpelem" id="u28296-4" data-muse-uid="U28296" data-muse-type="txt_frame"><!-- content -->
           <p>Privacy Policy</p>
          </div>
          <!-- /m_editable -->
          <!-- m_editable region-id="editable-static-tag-U28295-BP_infinity" template="umbrellas.html" data-type="html" data-ice-options="disableImageResize,link" -->
          <div class="clearfix grpelem" id="u28295-4" data-muse-uid="U28295" data-muse-type="txt_frame"><!-- content -->
           <p>Terms and Conditions</p>
          </div>
          <!-- /m_editable -->
         </div>
        </div>
       </div>
      </div>
     </div>
     <!-- m_editable region-id="editable-static-tag-U28293-BP_infinity" template="umbrellas.html" data-type="html" data-ice-options="disableImageResize,link" -->
     <div class="Footer-Contact-Link clearfix grpelem" id="u28293-7" data-muse-uid="U28293" data-muse-type="txt_frame"><!-- content -->
      <p>If you wish to contact us about designing and manufacturing unique umbrellas, wellington boots or any of our accessories then please use the phone number or email listed here. Or, fill in the contact form on our <span>Contact Us</span> page.</p>
     </div>
     <!-- /m_editable -->
     <!-- m_editable region-id="editable-static-tag-U28294-BP_infinity" template="umbrellas.html" data-type="html" data-ice-options="disableImageResize,link" -->
     <div class="clearfix grpelem" id="u28294-4" data-muse-uid="U28294" data-muse-type="txt_frame"><!-- content -->
      <p>All Rights Reserved. Grant Barnett 2016</p>
     </div>
     <!-- /m_editable -->
     <!-- m_editable region-id="editable-static-tag-U29136-BP_infinity" template="umbrellas.html" data-type="html" data-ice-options="disableImageResize,link,clickable" data-ice-editable="link" -->
     <a class="nonblock nontext transition clearfix grpelem" id="u29136-4" href="index.html" data-href="page:U26942" data-muse-uid="U29136" data-muse-type="txt_frame"><!-- content --><p>home</p></a>
     <!-- /m_editable -->
     <!-- m_editable region-id="editable-static-tag-U29138-BP_infinity" template="umbrellas.html" data-type="html" data-ice-options="disableImageResize,link,clickable" data-ice-editable="link" -->
     <a class="nonblock nontext transition clearfix grpelem" id="u29138-4" href="about-us.html" data-href="page:U28967" data-muse-uid="U29138" data-muse-type="txt_frame"><!-- content --><p>about us</p></a>
     <!-- /m_editable -->
     <!-- m_editable region-id="editable-static-tag-U29140-BP_infinity" template="umbrellas.html" data-type="html" data-ice-options="disableImageResize,link,clickable" data-ice-editable="link" -->
     <a class="nonblock nontext transition clearfix grpelem" id="u29140-4" href="wellies.html" data-href="page:U27400" data-muse-uid="U29140" data-muse-type="txt_frame"><!-- content --><p>wellies</p></a>
     <!-- /m_editable -->
    </div>

</div>
