<?php
$leftPath1 = realpath(dirname(__FILE__) . '/..' . $data['left-product']);
$image1Attr = getimagesize($leftPath1)[3];
?>
<div class="col-12 pre-title">
    <div class="row">
        <div class="col-6">
            <img <?= $image1Attr ?> class="pre-left-product" src="<?= $data['left-product'] ?>" alt="">
        </div>
        <div class="col-6 pre-title-falling">
            <img class="prt-fall-1" src="<?= $data['right-product'] ?>" alt="" srcset="">
            <img class="prt-fall-2" src="<?= $data['image'] ?>" alt="" srcset="">
        </div>
    </div>
</div>