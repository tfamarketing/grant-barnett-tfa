<div id="navigation-overlay">
    <div style="position: absolute; top: 0px; left: 0px;">
        <div id="u29357" style="left: 0px; top: 0px; width: auto; height: auto; padding: 0px; margin: 0px; z-index: auto;">
            <div class="overlayWedge" style="width: 100%; height: 100%;"></div>
        </div>
    </div>
    <span class="ContainerGroup actAsDiv rgba-background clearfix horizontalSlideShow" style="width: 1233px;height: 956px;position: absolute;padding: 0px;left: 50%;top: 50%;border-width: 0px;background: none;transform: translate(-50%, -50%);margin-left: 10%;">
    <!-- stack box -->
        <span class="Container actAsDiv clearfix grpelem wp-panel wp-panel-active" id="u29358" role="tabpanel" aria-labelledby="u29390" style="position: absolute; left: 0px; top: 0px; display: block;">
            <span class="actAsDiv grpelem" id="u29367"></span>
            <a class="nonblock nontext MuseLinkActive actAsDiv clearfix grpelem" id="u29377-4" href="index.html" data-href="page:U26942" data-muse-uid="U29377" data-muse-type="txt_frame"><span class="actAsPara">home</span></a>
            <a class="nonblock nontext actAsDiv transition clearfix grpelem" id="u29375-4" href="wellies.html" data-href="page:U27400" data-muse-uid="U29375" data-muse-type="txt_frame"><span class="actAsPara">wellies</span></a>
            <a class="nonblock nontext actAsDiv transition clearfix grpelem" id="u29381-4" href="about-us.html" data-href="page:U28967" data-muse-uid="U29381" data-muse-type="txt_frame"><!-- content --><span class="actAsPara">About us</span></a><!-- /m_editable --><!-- m_editable region-id="editable-static-tag-U29376-BP_infinity" template="index.html" data-type="html" data-ice-options="disableImageResize,link" data-ice-editable="link" -->
            <a class="nonblock nontext actAsDiv transition clearfix grpelem" id="u29376-4" href="contact.html" data-href="page:U28573" data-muse-uid="U29376" data-muse-type="txt_frame"><!-- content --><span class="actAsPara">contact</span></a>
            <a class="nonblock nontext actAsDiv transition clearfix grpelem" id="u29373-4" href="process.html" data-href="page:U2353" data-muse-uid="U29373" data-muse-type="txt_frame"><!-- content --><span class="actAsPara">process</span></a>
            <a class="nonblock nontext actAsDiv transition clearfix grpelem" id="u29362-4" href="umbrellas.html" data-href="page:U28064" data-muse-uid="U29362" data-muse-type="txt_frame"><!-- content --><span class="actAsPara">umbrellas</span></a>
            <a class="nonblock nontext actAsDiv transition clearfix grpelem" id="u29382-4" href="accessories.html" data-href="page:U26397" data-muse-uid="U29382" data-muse-type="txt_frame"><!-- content --><span class="actAsPara">accessories</span></a>
            <a class="nonblock nontext actAsDiv transition clearfix grpelem" id="footwearlink" href="footwear.php" data-href="page:U26397" data-muse-uid="U29382" data-muse-type="txt_frame"><!-- content --><span class="actAsPara">Footwear</span></a>
            <span class="actAsDiv grpelem" id="u29361"><!-- simple frame --></span>
            <span class="actAsDiv grpelem" id="u29383"><!-- simple frame --></span>
            <span class="actAsDiv grpelem" id="u29386"><!-- simple frame --></span>
            <span class="actAsDiv grpelem" id="u29374"><!-- simple frame --></span>
            <span class="actAsDiv grpelem" id="u29370"><!-- simple frame --></span>
            <span class="actAsDiv grpelem" id="u29378"><!-- simple frame --></span>
            <span class="actAsDiv grpelem" id="footwearlink-frame-thing"><!-- simple frame --></span>
            <span class="actAsDiv clip_frame grpelem" id="u29359" data-muse-uid="U29359" data-muse-type="img_frame"><!-- image --><img class="block" id="u29359_img" src="images/grant%20barnett%20-%20nav%20icons-27.png?crc=138196228" alt="" width="57" height="57" data-muse-src="images/grant%20barnett%20-%20nav%20icons-27.png?crc=138196228"></span>
            <span class="actAsDiv clip_frame grpelem" id="u29384" data-muse-uid="U29384" data-muse-type="img_frame"><!-- image --><img class="block" id="u29384_img" src="images/grant%20barnett%20-%20nav%20icons-29.png?crc=4276869417" alt="" width="57" height="57" data-muse-src="images/grant%20barnett%20-%20nav%20icons-29.png?crc=4276869417"></span>
            <span class="actAsDiv clip_frame grpelem" id="u29363" data-muse-uid="U29363" data-muse-type="img_frame"><!-- image --><img class="block" id="u29363_img" src="images/grantbarnett-accessoriesicon.png?crc=3852185675" alt="" width="57" height="57" data-muse-src="images/grantbarnett-accessoriesicon.png?crc=3852185675"></span>
            <span class="actAsDiv clip_frame grpelem" id="u29379" data-muse-uid="U29379" data-muse-type="img_frame"><!-- image --><img class="block" id="u29379_img" src="images/grantbarnett-homeicon.png?crc=27295160" alt="" width="57" height="57" data-muse-src="images/grantbarnett-homeicon.png?crc=27295160"></span>
            <span class="actAsDiv clip_frame grpelem" id="u29371" data-muse-uid="U29371" data-muse-type="img_frame"><!-- image --><img class="block" id="u29371_img" src="images/grantbarnett-processicon.png?crc=447050362" alt="" width="57" height="57" data-muse-src="images/grantbarnett-processicon.png?crc=447050362"></span>
            <span class="actAsDiv clip_frame grpelem" id="u29368" data-muse-uid="U29368" data-muse-type="img_frame"><!-- image --><img class="block" id="u29368_img" src="images/grantbarnett-welliesicon.png?crc=250828502" alt="" width="57" height="57" data-muse-src="images/grantbarnett-welliesicon.png?crc=250828502"></span>
            <span class="actAsDiv clip_frame grpelem" id="u29365" data-muse-uid="U29365" data-muse-type="img_frame"><!-- image --><img class="block" id="u29365_img" src="images/grant%20barnett%20-%20nav%20icons-28.png?crc=4080900105" alt="" width="57" height="57" data-muse-src="images/grant%20barnett%20-%20nav%20icons-28.png?crc=4080900105"></span>
            <span class="actAsDiv clip_frame grpelem" id="footwearlink-icon" data-muse-uid="U29365" data-muse-type="img_frame"><!-- image --><img class="block" id="u29365_img" src="images/WellyIcon2.png?crc=4080900105" alt="" width="57" height="57" data-muse-src="images/grant%20barnett%20-%20nav%20icons-28.png?crc=4080900105"></span>
            <span class="PamphletCloseButton PamphletLightboxPart popup_element actAsDiv rounded-corners transition clearfix" id="u29352" tabindex="0" role="button" aria-label="close" style="top: 822px; left: 533px;"><!-- group --><!-- m_editable region-id="editable-static-tag-U29353-BP_infinity" template="index.html" data-type="html" data-ice-options="disableImageResize,link" -->
                <span class="actAsDiv transition clearfix grpelem" id="u29353-4" data-muse-uid="U29353" data-muse-type="txt_frame"><!-- content -->
                    <span class="actAsPara">X</span>
                </span><!-- /m_editable -->
            </span>
        </span>
    </span>
</div>