<?php
$altLayout = false;
if (array_key_exists('optional-product2', $data)) {
    $altLayout = true;
}
$imagePath1 = realpath(dirname(__FILE__) . '/..' . $data['optional-product']);
$image1Attr = getimagesize($imagePath1)[3];
?>
<div class="col-12 title-optional">
    <div class="row">
        <div class="col-6">
            <img <?= $image1Attr ?> class="opt-img-1" src="<?= $data['optional-product'] ?>" alt="">
            <?php if ($altLayout): ?>
            <p class="txt-orange text left position-relative" style="top: 140px;">
                <?= $data['optional-text'] ?>
            </p>
            <img class="alt-layout-img" src="<?= $data['optional-product2'] ?>" alt="">
            <?php else: ?>
            <img class="opt-img-2 parallax" data-scroll-factor="-2" src="<?= $data['optional-image'] ?>" alt="">
            <?php endif; ?>
        </div>
        <div class="col-6">
            <?php if (!$altLayout): ?>
            <p class="txt-orange text right parallax" data-scroll-factor="-1.5">
                <?= $data['optional-text'] ?>
            </p>
            <?php else: ?>
            <img class="opt-img-2" src="<?= $data['optional-image'] ?>" alt="">
            <?php endif; ?>
        </div>
    </div>
</div>