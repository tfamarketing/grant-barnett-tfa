<?php
if ($data['text-side'] === 'right') {
    $offsetClass = 'offset-6 txt-orange';
} else {
    $offsetClass = 'txt-orange left';
}
?>
<div class="col-12 d-flex justify-content-center six-shoes">
    <?php $shoes = $data['shoes']; ?>
    <div class="box position-relative">
        <img class="shoe tl rotate" src="<?= $shoes['tl'] ?>" alt="" data-rotate-start="50" data-rotate-end="100" data-angle-start="-20" data-angle-end="20">
        <img class="shoe tm" src="<?= $shoes['tm'] ?>" alt="">
        <img class="shoe tr rotate" src="<?= $shoes['tr'] ?>" alt="" data-rotate-start="50" data-rotate-end="100" data-angle-start="20" data-angle-end="-20">
        <img class="shoe bl rotate" src="<?= $shoes['bl'] ?>" alt="" data-rotate-start="50" data-rotate-end="100" data-angle-start="-20" data-angle-end="20">
        <img class="shoe bm" src="<?= $shoes['bm'] ?>" alt="">
        <img class="shoe br rotate" src="<?= $shoes['br'] ?>" alt="" data-rotate-start="50" data-rotate-end="100" data-angle-start="20" data-angle-end="-20">
    </div>
</div>