<?php
error_reporting(0);
?>
<!DOCTYPE html>
<html class="nojs html" lang="en-GB">
<head>
    <meta http-equiv="Content-type" content="text/html;charset=UTF-8">
    <title>Footwear</title>
    <link rel="stylesheet" href="https://use.typekit.net/gbx6fqz.css">


    <script type="text/javascript">document.documentElement.className = document.documentElement.className.replace(/\bnojs\b/g, 'js');</script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="css/app.css?r<?= rand(0, 80) ?>">
    <?php include './functions.php'; ?>
</head>
<body>
    <div style="overflow-x:hidden;overflow-y:hidden;">

        <header>
            <img id="logo" src="images/grant%20barnet%20logo%20-%20white%20square-02.png?crc=445515127" alt="" width="52" height="52" data-muse-src="images/grant%20barnet%20logo%20-%20white%20square-02.png?crc=445515127"/>
            <span class="Thumb popup_element actAsDiv wp-tab-active" id="hamburger" role="button" tabindex="0" aria-haspopup="true" aria-controls="u29358">
                <img class="actAsDiv grpelem" id="h-icon" alt="" width="101" height="101" src="images/hamburger-03-u29391.png?crc=459128416">
            </span>
            <div class="tagline">
                <p>Any sizes, colours, patterns, styles.</p>
                <p>Your imagination is the limit.</p>
                <span id="scroll-down" class="scroll-down d-block"></span>
            </div>
            <div class="bg-container">
                <div class="bg active"></div>
                <div class="bg"></div>
                <div class="bg"></div>
                <div class="bg"></div>
                <div class="bg"></div>
                <div class="bg"></div>
            </div>
        </header>

        <section id="content" class="content container-fluid">
            <?= footwearSection('childrens-sliders', getData('footwear', 'childrens-sliders')); ?>
            <?= footwearSection('childrens-clogs', getData('footwear', 'childrens-clogs')); ?>
            <?= footwearSection('adult-sandals', getData('footwear', 'adult-sandals')); ?>
            <?= footwearSection('adult-sliders', getData('footwear', 'adult-sliders')); ?>
        </section>

    </div>

    <div class="size_browser_width" id="u27653"><!-- custom html -->
        <div class="panoramic-image">

        </div>
    </div>

    <?php include './includes/footer.php'; ?>

    <?php include './includes/nav-overlay.php' ?>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <script src="/scripts/footwear.js?r=<?= rand(0, 80) ?>"></script>
    <script src="/scripts/rotate.js?r=<?= rand(0, 80) ?>"></script>
    <script src="/scripts/bxslider.js"></script>
            <script>

                $(function() {

                    "use strict";

                    var scroll_mode_u27653 = "standardm";

					if(scroll_mode_u27653 == "standard") {
	                    $('#u27653 .bxslider').bxSlider({
	                    	mode: 'horizontal',
							minSlides: 1,
							maxSlides: 1,
							slideMargin: 0,
							ticker: true,
							speed: 50000,
							autoDirection: 'next'
						});
					}
					else {
						(function slide(){
						    $('#u27653 .panoramic-image').animate(
                                {backgroundPosition : '-=1px'}, 30, 'linear', slide);
						})();
					}

                });

            </script>
</body>
</html>