// need a list of elements that need to rotate
var rotateElems = $('.rotate');

function between(test, lower, upper) {
    return lower < test && test < upper;
}

function getPercentage (input, total) {
    return input / total * 100;
}

function getPercentageFromRange(input, max, min) {
    return (input - min) / (max - min) * 100;
}

function getInputFromPercentage(perc, total) {
    return (perc * total) / 100;
}

function getInputFromPercentageRange(perc, max, min) {
    return ((perc * (max - min)) / 100) + min;
}

function calculateRotation(input, start, end, start_angle, end_angle) {
    var perc = getPercentageFromRange(input, end, start);
    return getInputFromPercentageRange(perc, start_angle, end_angle);
}

function rotate() {
    var scrollTop = $(window).scrollTop(),
        winHeight = $(window).height();
    rotateElems.each(function() {
        var $this = $(this),
            thisWinTop = $this.offset().top - scrollTop,
            thisWinTopPerc = thisWinTop / winHeight * 100,
            rotateStart = parseInt($this.attr('data-rotate-start')),
            rotateEnd = parseInt($this.attr('data-rotate-end')),
            rotateAngleStart = parseInt($this.attr('data-angle-start')),
            rotateAngleEnd = parseInt($this.attr('data-angle-end'));

        if ($this.isInViewport() && between(thisWinTopPerc, rotateStart, rotateEnd)) {
            $this.css('transform', 'rotate(' + calculateRotation(thisWinTopPerc, rotateStart, rotateEnd, rotateAngleStart, rotateAngleEnd) + 'deg)');
        }

    });
}

$(document).ready(function() {
    rotateElems.each(function() {
        var rotation;
        var $this = $(this),
            thisWinTop = $this.offset().top - $(window).scrollTop(),
            thisWinTopPerc = thisWinTop / $(window).height() * 100,
            rotateStart = parseInt($this.attr('data-rotate-start')),
            rotateEnd = parseInt($this.attr('data-rotate-end')),
            rotateAngleStart = parseInt($this.attr('data-angle-start')),
            rotateAngleEnd = parseInt($this.attr('data-angle-end'));

        if (!$this.isInViewport() || !between(thisWinTopPerc, rotateStart, rotateEnd)) {
            if (thisWinTopPerc > rotateEnd) {
                rotation = rotateAngleStart;
            } else if (thisWinTopPerc < rotateStart) {
                rotation = rotateAngleEnd;
            }
        } else {
            rotation = calculateRotation(thisWinTopPerc, rotateStart, rotateEnd, rotateAngleStart, rotateAngleEnd);
        }
        $this.css('transform', 'rotate(' + rotation + 'deg)');
    })
    rotate();
    window.addEventListener("scroll", rotate, {passive: true});
})