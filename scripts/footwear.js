$.fn.isInViewport = function() {
    var elementTop = ($(this).offset().top);
    var elementBottom = elementTop + $(this).outerHeight();

    var viewportTop = $(window).scrollTop();
    var viewportBottom = viewportTop + $(window).height();

    return (elementBottom > viewportTop && elementTop < viewportBottom);
};

function offsetFromViewportCentre(e) {
    var eCentre, viewportCentre;

    eCentre = $(e).offset().top + (e.height / 2);
    viewportCentre = $(window).scrollTop() + (window.innerHeight / 2);

    return eCentre - viewportCentre;
}

function aboveViewportCentre (e) {
    return 0 <= offsetFromViewportCentre(e);
}

function calculateChange (e) {
    return $(e).offset().top / 10 * $(e).attr('data-scroll-factor');
}

function initiliaseTop(e) {
    $(e).attr('data-initial-top', parseFloat($(e).position().top));
}


function parallax() {
    parallaxImages.each(function() {
        var $this = $(this);
        newTop = $this.attr('data-initial-top') - (offsetFromViewportCentre(this) / 10 * $this.attr('data-scroll-factor'));
        $this.css('top', newTop);
    });
}

// calculate starting positions
var parallaxImages = $('img.parallax');

$(document).ready(function() {
    $('.popup_element').on('click', function() {
        $('#navigation-overlay').addClass('active');
    });
    $('#navigation-overlay').on('click', function() {
        $(this).removeClass('active');
    });
});

$(window).on("load", function() {
    parallaxImages.each(function() {
        initiliaseTop($(this));
    });

    // needs to run 6 times to put things in the right place
    for (var p = 0; p < 6; p++) {
        parallax();
    }

    document.addEventListener('scroll', parallax, {passive: true});
});

$("#scroll-down").click(function() {
    $([document.documentElement, document.body]).animate({
        scrollTop: $("#content").offset().top
    }, 2000);
});

// header slider

var headerBgs = $('.bg');
console.log(headerBgs);

function changeBg() {
    var $active = headerBgs.filter('.active');
    var $next = $active.next('.bg');

    if ($next.length == 0) {
        $next = headerBgs.first();
    }

    $active.removeClass('active');
    $next.addClass('active');
}

$(document).ready(function() {
    setInterval(changeBg, 5000);
})